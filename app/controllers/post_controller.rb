class PostController < ApplicationController
  include TwitterAPI

  def index
  end

  def send_tweet
    response = authenticate_and_send_tweet(params[:message])
    render text: "#{response}"
  end


  private

  # generate required signature and nonce to hit the twitter API for update
  def authenticate_and_send_tweet(message)
    consumer_key = "#{Figaro.env.consumer_key}"
    consumer_secret = "#{Figaro.env.consumer_secret}"

    tweet_text = url_encode("status=#{message}&include_entities=true")

    method = 'POST'
    uri = 'https://api.twitter.com/1.1/statuses/update.json'
    complete_uri = "https://api.twitter.com/1.1/statuses/update.json?#{tweet_text}"

    params = return_params(consumer_key)
    signature_base_string = signature_base_string(method, complete_uri, params)
    access_token = "#{Figaro.env.access_token}"

    signing_key = consumer_secret + '&' + "#{access_token}"
    params['oauth_signature'] = url_encode(sign(signing_key, signature_base_string))
    header_string = header(params)

    request_data(header_string, uri, method, tweet_text)
  end

  # make request
  def request_data(header, base_uri, method, post_data=nil)
    url = URI.parse(base_uri)
    http = Net::HTTP.new(url.host, 443)
    http.use_ssl = true

    if method == 'POST'
      resp, data = http.post(url.path, post_data, { 'Authorization' => header })
    else
      resp, data = http.get(url.to_s, { 'Authorization' => header })
    end
    resp.message
  end
end

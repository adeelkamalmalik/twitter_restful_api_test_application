module TwitterAPI
  def return_params(consumer_key)
    params = {
        'oauth_consumer_key' => consumer_key,
        'oauth_nonce' => get_nonce,
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_timestamp' => Time.now.getutc.to_i.to_s,
        'oauth_token' => "#{Figaro.env.access_token}",
        'oauth_version' => '1.1'
    }
  end

  def get_nonce(size=10)
    Base64.encode64(OpenSSL::Random.random_bytes(size)).gsub(/\W/, '')
  end

  def signature_base_string(method, uri, params)
    encoded_params = params.sort.collect{ |k, v| url_encode("#{k}=#{v}") }.join('%26')
    method + '&' + url_encode(uri) + '&' + encoded_params
  end

  def url_encode(string)
    CGI::escape(string)
  end

  def sign(key, base_string)
    digest = OpenSSL::Digest::Digest.new('sha1')
    hmac = OpenSSL::HMAC.digest(digest, key, base_string)
    Base64.encode64(hmac).chomp.gsub(/\n/, '')
  end

  def header(params)
    header = "OAuth "
    params.each do |k, v|
      header += "#{k}=\"#{v}\", "
    end
    header.slice(0..-3)
  end

  def parse_string(str)
    ret = {}
    str.split('&').each do |pair|
      key_and_val = pair.split('=')
      ret[key_and_val[0]] = key_and_val[1]
    end
    ret
  end
end
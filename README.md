# README #

This project is implementation for Twitter Restful API without a gem.

### Follow instructions to see code implementation ###

* Add file named "application.yml" in config/ directory with 
```
consumer_key, consumer_secret, access_token and access_token_secret
```
* Custom Twitter API library for Nonce, signature and signing purpose

```
lib/util/twitter_api.rb
```

* Twitter API methods calling
```
controllers/posts_controller.rb
```
* Another Approach:: Run script in ```public/get_user_tweets.sh``` (you'll need to set permissions for this file) to get tweets for your twitter account.